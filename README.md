#To test the app go to this Heroku url:
* <http://coveo.herokuapp..com>

##Instruction to run locally

* install ruby at version 2.2.4 ( or use rvm or rubyenv )
	* <https://www.ruby-lang.org/en/documentation/installation/>
	* <https://rvm.io/>
	* <https://github.com/rbenv/rbenv>
* Install bundler gem `gem install bundler`
* clone repository master branch `git clone https://patrick_meunier@bitbucket.org/patrick_meunier/coveo.git>`
* Get all the project gem's installed `bundle install`
* To be sure every gems is up to date `bundle update`
* start the app with : `rackup -p 5000 coveo.rb`
* open <localhost:5000> in your browser

##Dependencies
####Javascript
* Jquery <http://www.jequery.com>
* Elizabot.js <http://www.masswerk.at/elizabot/>
* Handlebars <http://handlebarsjs.com/>

####CSS
* Bootstrap 3 <http://www.getbootstrap.com>

####Ruby gems
* Sinatra web framework <http://www.sinatrarb.com/>
* Sinatra contrib for reloader extension
* Sinatra JSONP <https://github.com/shtirlic/sinatra-jsonp>
* Slim templating engine <http://slim-lang.com/>
* Slim-mustache to add mustache-handlebars to Slim <https://github.com/Dearon/Slim-Mustache>
* Twitter <https://sferik.github.io/twitter/>
* Twitter-text : not used yet. <https://github.com/twitter/twitter-text>
* Bundler dependencies manager <http://bundler.io/>
* Thin ruby web server <http://code.macournoyer.com/thin/>

##Test json data
Two files included in `/public` to test Twitter search and trend json response without tapping the application request limit.

##Changes
#####Version 0.1:
* No error catching/rescueing for now.
* Fetching Canada's keyword trends implemented.
* Searching the selected keyword/hastag.
* Put the result of search on the page.

#####Version 0.2
* Added Eliza feeding with the tweet message content and print it's answer on the page.

#####Version 0.3
* Added Text to Voice with Responsive Voice API. We feed Eliza answer to the API and it speaks them.
* Added pause/resume control. It get's old after a while....

##TODO

* <strike>Feed tweet text to Elizabot.js</strike>
* <strike>Text to voice the Elizabot.js response with Responsive Voice API</strike>
* Implement Twitter stream API instead of REST API to monitor in real time the tweets from the selected trend.
*  Test with SpeechSynthesisVoice from Javascript native library.
*  Select voice language based on the Tweet's language.
*  Get the Bootstrap grid to even the panel disposition
*  Feed only #Hashtag to the select input after trend API call. Use twitter-text or a Regex expression