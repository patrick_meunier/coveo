#require Base to run a Modular style app.
require "sinatra/base"
require "sinatra/reloader"
require "sinatra/jsonp"
require "twitter"
require "slim"
require "slim/mustache"
require "json"


class Coveo < Sinatra::Base
  # register JSONP helper
  helpers Sinatra::Jsonp
  # Enable JSON pretty output...
  # enable :json_pretty

  # use sinatra::Reloader in development environment.
  configure :development do
    register Sinatra::Reloader
  end

  #http://recipes.sinatrarb.com/p/middleware/rack_commonlogger
  configure :production, :development do
    enable :logging
    file = File.new("#{settings.root}/log/#{settings.environment}.log", 'a+')
    file.sync = true
    use Rack::CommonLogger, file
  end

  # set pretty code output for slim.
  configure do
    set :slim, pretty: true
  end

  # We are using application-only auth, so we need only those api key. No user context implied.
  # Client configuration for the api.
  @@twitter_api_request = Twitter::REST::Client.new do |config|
    config.consumer_key = 'CHwZZ51rmeKoAJYqrfI4Sl8Yd'
    config.consumer_secret = 'M3B5eiRDxJrTNvveCQCaD2SU8wP4xTsECBbWGBFgDUox11pvzK'
  end

  #http://podtynnyi.com/2010/05/28/sinatra-jsonp-output-helper/
  get '/' do
    #using slim templating language to render the index page.
    slim :index
  end

  # https://github.com/t-p/twitter-trends/blob/master/twitter_trends.rb has good examples on using the trend api call
  # using trend_place because it's closer to the real api call
  get '/select_hashtag' do
    # jsonp helper returns the output
    #get Canada's trends. Returns 50 objects.
    # This call is cached for 5 minutes.
    api_data = @@twitter_api_request.trends_place(23424775).attrs[:trends]
    jsonp api_data
  end

  get '/search' do
    hashtag = params[:hashtag][:form_data]
    hashtag_search = @@twitter_api_request.search(hashtag).to_h

    #hash = File.read('public/search.json')

    jsonp hashtag_search

  end
  # \S*#(?:\[[^\]]+\]|\S+)
  def extract_hashtag(json)
    
  end
end