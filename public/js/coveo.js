$( document ).ready(function() {

  function voice_it_baby(message){
    responsiveVoice.speak(message, "US English Female");
  };

  function feed_eliza(message){
    var eliza= new ElizaBot();
    var reply = eliza.transform(message);

    return(reply);
  };
/*
  $('button').click(function(){
    if(responsiveVoice.isPlaying()){
      //$('button').text("Pause");
      responsiveVoice.pause(); 
    } else {
      //$('button').text("Play");
      responsiveVoice.resume();
    }
  });
  */
  $("form").submit(function(event){
    
    // GET data from the form select
    var form_data = $("select").val();
    // Grab the template script
    var template_code = $("#tweet-template").html();
    // Compile the template with handlebars
    var template = Handlebars.compile(template_code);
    // Retrieve placeholder
    var placeholder = $(".row");

    $.ajax({
      url: "/search",
      data: { hashtag: {form_data}},
      datatype: "jsonp",
      type: "GET"
    })
      // using the done promise callback
      .done(function(data){
        console.log(data);
        $.each(data.statuses, function( key, value ){
          // Get Eliza response to tweet
          var eliza_answer = feed_eliza(value.text);
          // Add Eliza response to html array
          value["eliza"] = eliza_answer;

          // Generate the html for each post
          var html = template(value);

          //Add attribute to panel for identification
          $(".panel.panel-info").attr("id", key );
          // Render the tweet into the page
          placeholder.prepend(html);
          //Feed Eliza response to  text to voice service Responsive Voice.
          voice_it_baby(eliza_answer);

          //remove for production
          //console.log(feed_eliza);
          //console.log(html);
          //console.log(value.eliza);
        });
      })
      // using the fail promise callback
      .fail(function(data){
        alert("There is a problem with the search request.");
        //remove for production
        //console.log(data);
      });
    // stop the form from submitting the normal way and refreshing the page
    event.preventDefault();
  });

  $.ajax({
    url: "/select_hashtag",
    dataType: "jsonp",
    success: function( data ){
      console.log(data);
      $.each(data , function( key, value ) {
        $("select").append($('<option>').text(value.name).attr('value', value.name));
      });
    },
    error: function(){
      alert("There is a problem with the API request.");
      //remove for production
      //console.log(data);
    }
  });
});